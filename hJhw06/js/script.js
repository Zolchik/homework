'use strict'
const defineIpUrl = 'https://api.ipify.org/?format=json';
const defineByIpUrl = 'http://ip-api.com/json/';

document.querySelector('.btn').addEventListener('click', async () => {
    const currentIp = (await (await fetch(defineIpUrl)).json())['ip'];
    const userInfo = (await (await fetch(defineByIpUrl+currentIp)).json());
    const {timezone,country,city,regionName,region,zip} = userInfo;
    new Card({timezone,country,city,regionName,region,zip}).add(document.querySelector('.user-info'));
})

class Card {
    #infoObj
    constructor(infoObj) {
        this.#infoObj = infoObj;
        this.html = this.#render();
    }

    #render() {
        const divCard = document.createElement('div');
        divCard.classList.add('card');
        for (const key in this.#infoObj) {
            const p = document.createElement('p');
            p.innerText = key +': ' + this.#infoObj[key].toUpperCase();
            divCard.append(p);
        }
        return divCard;
    }
    add(node) {
        node.innerHTML = '';
        node.append(this.html);
    }
}