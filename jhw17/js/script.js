"use strict"

const student = {name: "", lastName: ""}

function getConsoleNumber(whatToGet = "some text", from = -Number.MAX_SAFE_INTEGER, to = Number.MAX_SAFE_INTEGER) {
    let number;
    do {
        number = prompt("Please input " + whatToGet);
    } while (!number || isNaN(+number) || +number < from || +number > to)
    return +number;
}

function getConsoleTextWithBreak(whatToGet = "some text", minLength = 2, isBreak = false, symbolBreak = null) {
    let text;
    do {
        text = prompt("Please input " + whatToGet);
        if (isBreak && text === symbolBreak) {
            break;
        }
    } while (!text || text.trim().length < minLength)

    return text;
}

student.countBadRating = function () {
    let countBadRating = 0;
    let isRating = false;
    for (const subject in this.tabel) {
        isRating = true;
        let rating = this.tabel[subject];
        if (rating < 4) {
            countBadRating++;
        }
    }
    return countBadRating;
};
student.gPA = function () {
    let countRating = 0;
    let counter = 0;
    for (const subject in this.tabel) {
        let rating = this.tabel[subject];
        counter++;
        countRating = countRating + rating;
    }
    if (!(counter === 0)) {
        return (countRating / counter);
    } else {
        return 0;
    }
};

student.name = getConsoleTextWithBreak("your Name");
student.lastName = getConsoleTextWithBreak("your LastName");

student.tabel = {};
let subject;
const stopSymbol = null;
do {
    subject = getConsoleTextWithBreak("subject name", 2, true, stopSymbol);
    if (!(subject === stopSymbol)) {
        student.tabel[subject] = getConsoleNumber("subject rating", 0, 10);
    }
} while (!(subject === stopSymbol))

const gPA = student.gPA();
const badRating = student.countBadRating();

if (!(gPA === 0) && badRating === 0) {
    console.log("Студент " + student.name + " " + student.lastName + " переведений на наступний курс");
}
if (gPA > 7 && badRating === 0) {
    console.log("Студенту " + student.name + " " + student.lastName + " призначено стипендію.");
}