"use strict"

const speedTeamMembers = [3, 7, 5, 6, 1];//стор поінтів
const backlog = [20, 10, 18, 45, 105, 30, 73];
const deadLine = "2022-08-24";
let deadLineDate = new Date(Date.parse(deadLine));
deadLineControl(speedTeamMembers, backlog, deadLineDate);

function sumArray(array){
    return array.reduce((accum, currentValue) => accum + currentValue);
}

function deadLineControl(speedArray, backlogArray, deadLineDate) {
    const speedArraySum = sumArray(speedArray);
    const backlogArraySum = sumArray(backlogArray);
    let daysForFinished = backlogArraySum / speedArraySum;
    let dayCounter = new Date();
    deadLineDate.setHours(0, 0, 0, 0);
    dayCounter.setHours(0, 0, 0, 0);

    while (daysForFinished > 0 && deadLineDate >= dayCounter) {
        if (dayCounter.getDay() === 0 || dayCounter.getDay() === 6) {
            dayCounter.setDate(dayCounter.getDate() + 1);
        } else {
            daysForFinished--;
            if (daysForFinished > 0) {
                dayCounter.setDate(dayCounter.getDate() + 1);
            }
        }
    }
    if (daysForFinished <= 0) {
        console.log(`Усі завдання будуть успішно виконані за ${(deadLineDate - dayCounter) / (1000 * 60 * 60 * 24)} днів до настання дедлайну!`);
    } else {
        console.log(`Команді розробників доведеться витратити додатково ${Math.ceil(daysForFinished * 8)}  годин після дедлайну, щоб виконати всі завдання в беклозі`);
    }
}

