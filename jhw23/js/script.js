"use strict"

document.querySelector("input").addEventListener("focus", () => {
    deleteMassageSpan();
})

document.querySelector("input").addEventListener("blur", () => {
    parseInt(document.querySelector("input").value) >= 0
        ? (createPriceSpan(document.querySelector("input").value), deletePrice())
        : createErrorSpan();
})

function deletePrice() {
    document.querySelector("button").addEventListener("click", () => {
        document.querySelector(`[data-currentPrice]`).remove();
        document.querySelector("input").value = "";
    })
}

function deleteMassageSpan() {
    document.querySelectorAll("[data-status], [data-currentPrice]").forEach(element => element.remove());
    document.querySelector("input").classList.remove("error");
    document.querySelector("input").classList.remove("current-price");
}

function createPriceSpan(price) {
    let span;
    if (!document.querySelector(`[data-currentPrice]`)) {
        span = document.createElement("span");
        document.querySelector("label").before(span);
        span.setAttribute("data-currentPrice", "active")
    } else {
        span = document.querySelector(`[data-currentPrice]`)
    }
    span.insertAdjacentHTML("beforeend", `Поточна ціна: ${price} <button><img src="img/x.png" alt = "x"></button>`)
    document.querySelector("input").classList.add("current-price");
}

function createErrorSpan() {
    let span
    span = document.createElement("span");
    document.querySelector("label").after(span);
    span.setAttribute("data-status", "error");
    span.innerText = "Please enter correct price";
    document.querySelector("input").classList.add("error");
}