"use strict"

const imgCount = document.body.querySelectorAll(".images-wrapper img").length;
let imgIndex = 1;

function slider() {
    const activeImg = document.body.querySelector(`[data-status = "active"]`);
    if (activeImg) {
        activeImg.dataset.status = "";
        document.body.querySelectorAll(".images-wrapper").forEach((element, index) => {
            if (element === activeImg) imgIndex = index;
        })
    }
    document.body.querySelector(`.images-wrapper :nth-child(${imgIndex})`).dataset.status = "active"
    imgIndex = imgIndex < imgCount ? imgIndex + 1 : 1;
}

function changeButton() {
    const button = document.querySelector("button");
    button.dataset.status === "stop"
        ? (button.dataset.status = "go", button.innerText = "Відновити показ")
        : (button.dataset.status = "stop", button.innerText = "Припинити показ")
}

let timeOutID = setInterval(slider, 3000);
let stop = false;
document.body.querySelector("button").addEventListener("click", () => {
    stop = !stop;
    stop ? clearInterval(timeOutID) : timeOutID = setInterval(slider, 3000);
    changeButton();
})