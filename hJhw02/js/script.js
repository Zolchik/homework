const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function addList(arrOfObject) {
    const requiredProp = ['name', 'author', 'price'];
    const ulList = document.querySelector('#root').appendChild(document.createElement("ul"));
    arrOfObject.forEach(el => {
        try {
            requiredProp.forEach(prop => {
                if (el[prop] === undefined) {
                    throw Error(`The ${prop} is undefined`)
                }
            })
            const liElement = ulList.appendChild(document.createElement('li'));
            liElement.innerText = el.name + " / author: " + el.author + " / price: " + el.price;
        } catch (err) {
            console.log(err.message);
        }
    })
}

addList(books);