"use strict"
function getConsoleNumber() {
    let number = prompt("Enter a number");
    while (!number || !Number.isInteger(+number)) {
        number = prompt("Enter correct number", number);
    }
    return +number;
}

function getConsoleMathOperation() {
    const mathOperationArray = [`+`, `-`, `*`, `/`];
    let mathOperation = prompt("Enter a MathOperation " + mathOperationArray);
    while (!mathOperation || !(mathOperationArray.includes(mathOperation))) {
        mathOperation = prompt("Enter correct operation " + mathOperationArray, mathOperation);
    }
    return mathOperation;
}

function performMathOperation(arg1, arg2, operation) {
    switch (operation) {
        case "+":
            return arg1 + arg2;
        case "-":
            return arg1 - arg2;
        case "*":
            return arg1 * arg2;
        case "/":
            return arg1 / arg2;
    }
}

console.log(performMathOperation(getConsoleNumber(), getConsoleNumber(), getConsoleMathOperation() ));