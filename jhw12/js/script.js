"use strict"

document.body.querySelector(".btn-wrapper").addEventListener("click", e => {
    turnBlue(e, "click")
})

document.body.addEventListener("keyup", turnBlue)

function turnBlue(e, way = "keyup") {
    const currentActive = document.body.querySelector('[data-status=active]');
    if (currentActive) {
        currentActive.setAttribute("data-status", "");
        currentActive.style.backgroundColor = "";
    }
    document.body.querySelectorAll(".btn").forEach(element => {
        const key = way === "keyup" ? e.code : e.target.getAttribute("data-name");
        if (element.getAttribute("data-name") === key) {

            element.setAttribute("data-status", "active");
            element.style.backgroundColor = "blue";
        }
    })
}