"use strict"

function eyeReverse(node) {
    const visible = {true: "fa-eye", false: "fa-eye-slash"}
    const type = {true: "password", false: "text"}
    let bool;
    const passNode = document.querySelector(`[data-name=${node.getAttribute("data-name")}]`);

    for (const key in type) {
        if (type[key] === passNode.getAttribute("type")) {
            bool = key === "true";
            break;
        }
    }
    passNode.setAttribute("type", type[!bool]);
    node.classList.replace(visible[bool], visible[!bool]);
}

function getError() {
    let errorMassage = document.createElement("p");
    document.querySelector(".btn").before(errorMassage);
    errorMassage.textContent = "Потрібно ввести однакові значення. Мінімум 3 символа";
    errorMassage.style.color = "red";
    errorMassage.classList.add("error");
}

document.querySelectorAll('.icon-password').forEach(element => {
    element.addEventListener("click", e=>{
        eyeReverse(e.target)

    })
})

document.querySelector("form").addEventListener("submit", () => {
    let passArr = document.querySelectorAll('.password');
    passArr[0].value === passArr[1].value && passArr[0].value.length > 2 ? alert("You are welcome") : getError();
    passArr[0].value = passArr[1].value = "";
})

document.querySelectorAll('.password').forEach(element => {
    element.addEventListener("focus", () => {
        document.querySelector(".error") ? document.querySelector(".error").remove() : "";
    })
})