"use strict"
function nthFibonacci(f0 = 0, f1 = 1, n) {
    if (n === 1) return f1;
    if (n === 0) return f0;
    if (n > 2) {
        return nthFibonacci(f0, f1, n - 1) + nthFibonacci(f0, f1, n - 2);
    } else if (n === 2) return f1 + f0;
    if (n < -1) {
        return nthFibonacci(f0, f1, n + 2) - nthFibonacci(f0, f1, n + 1);
    } else if (n === -1) return f1 - f0;
}

function getConsoleNumber() {
    let number = prompt("Enter a number");
    while (!number || !Number.isInteger(+number)) {
        number = prompt("Enter correct number", number);
    }
    return +number;
}

console.log(nthFibonacci(undefined, undefined, getConsoleNumber()));

