"use strict"

function createNewGameForm() {
    const div = document.body.appendChild(document.createElement("div"));
    div.classList.add("newGame");
    div.insertAdjacentHTML("afterbegin", '<input type="number" value="3" min="3" max="20">');
    const button = div.appendChild(document.createElement("button"));
    button.innerText = "Start new game";
    return button;
}

function createBoard(sizeBoard) {
    const board = document.body.appendChild(document.createElement("div"));
    board.classList.add("board");
    setStyleBoard(sizeBoard);
    for (let i = 0; i < sizeBoard * sizeBoard; i++) {
        board.appendChild(document.createElement("div")).classList.add("close");
    }
    return board;
}

function generateMine(sizeBoard) {
    let count = 0;
    const countMine = Math.round(sizeBoard * sizeBoard / 6);
    while (count < countMine) {
        const divForMine = document.querySelector(`.board :nth-child(${randomNumber(1, sizeBoard * sizeBoard)})`);
        if (!divForMine.classList.contains("mine")) {
            divForMine.classList.add("mine");
            count++;
        }
    }
}

function randomNumber(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function addMine() {
    document.querySelectorAll(".mine").forEach(element => {
        if (element.querySelector('[alt="flag"]')) return;
        element.insertAdjacentHTML("beforeend", '<img src="img/bomb.png" alt=" ">')
    })
}

function neighbourCellArray(cell, sizeBoard) {
    sizeBoard = +sizeBoard;
    if (cell > sizeBoard * sizeBoard || cell < 1) return [];
    let neighbour = [cell];
    if (cell - sizeBoard >= 1) neighbour.push(cell - sizeBoard);
    if ((cell + sizeBoard) <= (sizeBoard * sizeBoard)) neighbour.push(cell + sizeBoard);
    neighbour.forEach(cell => {
        if (Math.trunc((cell) / sizeBoard) === Math.trunc((cell - 1) / sizeBoard) && cell + 1 <= sizeBoard * sizeBoard) neighbour.push(cell + 1);
        if (Math.trunc((cell - 2) / sizeBoard) === Math.trunc((cell - 1) / sizeBoard) && cell - 1 >= 1) neighbour.push(cell - 1);
    })
    return neighbour;
}

function neighbourCloseCellArray(neighbourArray) {
    return neighbourArray.filter((neighbour) => {
        return document.body.querySelector(`.board :nth-child(${neighbour})`).classList.contains("close");

    })
}

function openNeighbour(indexCell, sizeBoard) {
    const neighbourArray = neighbourCellArray(indexCell, sizeBoard);
    let countMine = mineCounter(indexCell, neighbourArray);
    const targetDiv = document.body.querySelector(`.board :nth-child(${indexCell}`);
    targetDiv.classList.replace("close", "open");
    if (countMine > 0) {
        targetDiv.innerText = countMine;
    } else {
        neighbourCloseCellArray(neighbourArray).map(neighbour => {
                const neighbourDiv = document.body.querySelector(`.board :nth-child(${neighbour})`);
                neighbourDiv.classList.replace("close", "open");
                return neighbour;
            }
        ).forEach(blank => openNeighbour(blank, sizeBoard));
    }
}

function mineCounter(indexCell, neighbourArray) {
    let countMine = 0;
    neighbourArray.forEach(cell => {
        if (document.body.querySelector(`.board>:nth-child(${cell})`).classList.contains("mine")) countMine++;
    })
    return countMine;
}

function scoreBoard(countMine) {
    const div = document.createElement("div");
    div.classList.add("scoreboard")
    div.appendChild(document.createElement("div"));
    div.appendChild(document.createElement("div"));
    document.querySelector(".board").before(div);
    div.querySelector(":last-child").innerText = countMine;
    div.querySelector(":first-child").innerText = 0;
    return div;
}

function setStyleBoard(sizeBoard) {
    const board = document.querySelector(".board");
    board.style.height = 32 * sizeBoard + "px";
    board.style.width = 32 * sizeBoard + "px";
    board.style.gridTemplateColumns = `repeat(${sizeBoard}, auto)`;
    board.style.gridTemplateRows = `repeat(${sizeBoard}, auto)`;
}

const button = createNewGameForm();

button.addEventListener("click", () => {
    const sizeBoard = +document.body.querySelector(".newGame>input").value;
    button.disabled = true;
    document.body.querySelectorAll(".board, .scoreboard").forEach(element => element.remove());
    const countMine = Math.round(sizeBoard * sizeBoard / 6);
    const openCell = sizeBoard * sizeBoard - countMine;
    const board = createBoard(sizeBoard);
    const scoreboard = scoreBoard(countMine);
    generateMine(sizeBoard);

    board.addEventListener("click", e => {
        if (e.target.classList.contains("open") || e.target.classList.contains("board") || board.querySelectorAll(".open").length === openCell) return;

        button.disabled = false;
        if (e.target.classList.contains("mine")) {
            addMine();
            board.querySelectorAll(".close").forEach(div => div.classList.replace("close", "open"))
        } else {
            let indexCell;
            board.querySelectorAll("div").forEach((element, index) => {
                if (e.target === element) indexCell = index + 1;
            })
            openNeighbour(indexCell, sizeBoard);
        }
        if (board.querySelectorAll(".open").length === openCell) {
            board.querySelectorAll(".close").forEach(element => {
                element.innerHTML = '<img src="img/flag.png" alt="flag">';
            })
        }
    });

    board.addEventListener("contextmenu", e => {
        e.preventDefault();
        if (e.target.classList.contains("open") || e.target.classList.contains("board") || board.querySelectorAll(".open").length === openCell) return;

        if (e.target.tagName === "DIV" && e.target.querySelector(":first-child")) e.target.querySelector(":first-child").remove();
        e.target.getAttribute("alt")
            ? e.target.remove()
            : e.target.insertAdjacentHTML("beforeend", '<img src="img/flag.png" alt="flag">');
        scoreboard.querySelector(":first-child").innerText = document.querySelectorAll('[alt="flag"]').length;
    })
})