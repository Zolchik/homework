"use strict"

function noContent() {
    document.querySelectorAll(".tabs-content *").forEach(liElement => {
        liElement.style.display = "none";
    })
}

function onForActiveContent(){
    const activeTab = document.querySelector(".active").getAttribute("_tab");
    document.querySelector(`.tabs-content [_tab=${activeTab}]`).style.display = "";
}

noContent();
onForActiveContent();

document.querySelector(".tabs").addEventListener("click", event => {
    document.querySelectorAll(".tabs-title").forEach(tabTitle => {
        if (event.target === tabTitle) {
            tabTitle.classList.add("active")
            noContent();
            onForActiveContent();
        } else {
            tabTitle.classList.remove("active")
        }
    })
})