"use strict"
const user = createNewUser();
console.log(user.getLogin());

function createNewUser() {
    return {
        _firstName: getConsoleText("user first name"),
        _lastName: getConsoleText("user last name"),
        getLogin() {
            return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
        },
        set setFirstName(firstName) {
            this._firstName = firstName;
        },
        set setLastName(lastName) {
            this._lastName = lastName;
        }
    }
}

function getConsoleText(whatToGet = "some text") {
    let text;
    do {
        text = prompt("Please input " + whatToGet);
    } while (!text || text.trim().length < 2)
    return text;
}
