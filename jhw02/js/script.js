"use strict"

let name = prompt("Input your name");
while(!name || parseInt(name) || name.length<2){
    name = prompt("Input your correct name", name);
}

let age = prompt("Input your age");
while(!age || Number(age)< 0 || Number(age) > 150 || isNaN(age)){
    age = prompt("Input your correct age", age);
}

switch (true){
    case (age<18):
        console.log("You are not allowed to visit this website.");
        break;
    case (age<=22):
        if(confirm("Are you sure you want to continue?"))
            console.log("Welcome, " + name);
        else
            console.log("You are not allowed to visit this website");
        break;
    default:
        console.log("Welcome, " + name);
        break;
}
