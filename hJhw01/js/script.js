class Employee {
    #name;
    #age;
    #salary;

    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(name) {
        this.#name = name;
    }

    get name() {
        return this.#name;
    }

    set age(age) {
        this.#age = age;
    }

    get age() {
        return this.#age;
    }

    set salary(salary) {
        this.#salary = salary;
    }

    get salary() {
        return this.#salary;
    }
}

class Programmer extends Employee {
    #lang;
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;

    }

    set salary(salary){
        super.salary = salary;
    }
    get salary() {
        return super.salary * 3;
    }

    set lang(lang) {
        this.#lang = lang;
    }
    get lang() {
        return this.#lang;
    }
}

const programmer001 = new Programmer('Polina',28, 2000, ['java', 'java script','php']);
const programmer002 = new Programmer('Nikola',35, 2500, ['perl', 'lisp','prolog']);
const programmer003 = new Programmer('Helga',40, 3200, ['assembler', 'c++']);

console.log(programmer001, programmer002, programmer003);
// console.log(programmer001.salary+'$', programmer002.salary+'$', programmer003.salary+'$');
// programmer001.lang = ['c#','pascal'];
// programmer002.name = "Tolic";
// programmer003.age = 45;
// programmer002.salary = 1500;
// console.log(programmer001, programmer002, programmer003);
// console.log(programmer001.salary+'$', programmer002.salary+'$', programmer003.salary+'$');
