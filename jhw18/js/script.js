"use strict"

const student = {
    name: "Olia",
    lastname: "Zaderii",
    birthYear: "1983",
    tabel: {
        math: [10, 7, 9],
        bio: 7,
        eng: {term01: 10, term02: 7, term03: 7},
    },
    group: "p05",
    gPA: function(){
        return "I counting";
    },
    counting(){
        return "I still counting";
    }
}
function cloneObject(baseObj, newObj = {}){
    for (const baseObjKey in baseObj) {
        if(typeof(baseObjKey) ==="object"){
            cloneObject(baseObjKey, newObj[baseObjKey]);
        }else{
            newObj[baseObjKey] = baseObj[baseObjKey];
        }

    }
    return newObj;
}
console.log(student);
const newStudent = cloneObject(student);
console.log(newStudent);

