"use strict"

//Services Navigation
{
    function offActive() {
        document.querySelectorAll(".services .active").forEach(element => {
            element.classList.remove("active");
        })
    }
    document.querySelector(".services-nav").addEventListener("click", e => {
        if (e.target.tagName === "LI") {
            offActive();
            e.target.classList.add("active");
            document.querySelector(`.services-content[data-name=${e.target.dataset.name}]`).classList.add("active");
        }
    })
}

//Work gallery
{
    let imgIndex = 0;
    let loadCounter = 1;
    const imgSrcArray = shuffle(document.querySelectorAll(".work-gallery>img"));
    let category = "";
    let imgSrcArrayByCategory = imgSrcArray.slice(0);
    const imgBlock = document.querySelector(".work-gallery-img");
    clearGallery();
    createImgBlock(12);


    document.querySelector(".work .button").addEventListener("click", () => {
        document.querySelector(".work .button").style.opacity = "0";
        document.querySelector(".work .button").disabled = true;
        createLoadLine();
        showLoad();
        setTimeout(()=>{
            createImgBlock(12);
            setWorkHeight();
        },4000);
    })

    document.querySelector(".work-nav").addEventListener("click", e => {
        if (e.target.tagName === "LI" && !e.target.classList.contains("active")) {
            document.querySelector(".work .button").disabled = false;
            document.querySelector(".work-nav .active").classList.remove("active");
            e.target.classList.add("active");
            setCategory(e.target.innerText.toLowerCase());
            clearGallery();
            createImgBlock(12);
            setWorkHeight();
            setButtonOpacity();
        }
    })

    function createLoadLine(){
        const div = document.createElement("div");
        div.classList.add("load-line");
        for (let i = 0; i < 4; i++) {
            let child = document.createElement("div");
            div.append(child);
        }

        document.querySelector(".work-container>button").before(div)


    }

    function showLoad() {
        document.querySelectorAll(`.load-line>div`).forEach(element=>{
            element.style.opacity  = "0.1";
        })
        document.querySelector(`.load-line>div:nth-child(${loadCounter % 4 + 1})`).style.opacity  = "1";
        loadCounter ++;
        if(loadCounter>19) {
            document.querySelector(`.load-line`).remove();
            loadCounter = 1;
            return;
        }
        setTimeout(showLoad, 200);
    }

    function shuffle(collection) {
        return Array.from(collection).sort(() => Math.random() - 0.5).map(element =>
            element.getAttribute("src"));
    }

    function clearGallery() {
        document.querySelectorAll(".work-gallery>*").forEach(element => element.remove());
        imgIndex = 0;
    }

    function createImgBlock(imgCount = 0) {
        for (let i = 0; i < imgCount && imgIndex < imgSrcArrayByCategory.length; i++) {
            createImg(imgSrcArrayByCategory[imgIndex]);
        }
    }

    function createImg(src) {
        const copyImgBlock = imgBlock.cloneNode(true);
        copyImgBlock.querySelector("img").setAttribute("src", src);
        document.querySelector(".work-gallery").append(copyImgBlock);
        imgIndex++;
    }

    function setWorkHeight() {
        const multiplier = Math.ceil(imgIndex / 4);
        document.querySelector(".work").style.height = 600 + 200 * multiplier + "px";
        document.querySelector(".work-gallery").style.height = 200 * multiplier + "px";
    }

    function setCategory(inner){
        if (inner === "all") {
            category = "";
            imgSrcArrayByCategory = imgSrcArray.slice(0)
        } else {
            category = inner;
            imgSrcArrayByCategory = imgSrcArray.filter(src => src.includes(category))
        }
    }

    function setButtonOpacity(){
        const imgCounter = imgSrcArrayByCategory.length;
        let button = document.querySelector(".work .button");
        if (button.style.opacity === "0" && imgCounter > 12) button.style.opacity = "1";
        if (button.style.opacity !== "0" && imgCounter <= 12) button.style.opacity = "0";
    }

}

//People say gallery
{
    const imgPeopleSrcArray = getSrcBase();
    const imgPeopleSrcArrayLength = imgPeopleSrcArray.length;
    let imgPointer = 0;
    loadSlider();
    clickNextImage();
    clickPreviousImage();

    function loadCurrentPersonImg() {
        document.querySelector(".people-say-img").setAttribute("src", imgPeopleSrcArray.at(imgPointer - 3));
    }

    function getSrcBase() {
        let array = [];
        document.querySelectorAll(".people-say-img-box img").forEach((img, index) => {
            array[index] = img.getAttribute("src");
            img.remove();
        })
        return array;
    }

    function clickNextImage() {
        document.querySelector(".people-say-slider-nav:last-child").addEventListener("click", () => {
            document.querySelectorAll(".people-say-img-box img").forEach((img) => {
                img.style.left = parseInt(img.style.left) - 100 + "px";
            })
            imgCreator(imgPeopleSrcArray[imgPointer], ".people-say-img-box").style.left = "400px";
            document.querySelector(".people-say-img-box img:first-child").remove();
            document.querySelector(".people-say-img-box img:nth-child(4)").style.top = "0px";
            document.querySelector(".people-say-img-box img:nth-child(3)").style.top = "";
            nextPointer();
            loadCurrentPersonImg();
            activeCurrentPersonInfo();
        })
    }

    function clickPreviousImage() {
        document.querySelector(".people-say-slider-nav:first-child").addEventListener("click", () => {
            document.querySelectorAll(".people-say-img-box img").forEach((img) => {
                img.style.left = parseInt(img.style.left) + 100 + "px";
            })
            let img = imgCreator(imgPeopleSrcArray.at(imgPointer - 7));
            document.querySelector(".people-say-img-box").prepend(img);
            document.querySelector(".people-say-img-box img:last-child").remove();
            img.style.left = "-100px";
            document.querySelector(".people-say-img-box img:nth-child(4)").style.top = "0px";
            document.querySelector(".people-say-img-box img:nth-child(5)").style.top = "";
            nextPointer("previous");
            loadCurrentPersonImg();
            activeCurrentPersonInfo();
        })
    }

    function nextPointer(position = "next") {
        if (position === "previous") {
            imgPointer < 1 ? imgPointer = imgPeopleSrcArrayLength - 1 : imgPointer--;
        }
        if (position === "next") {
            imgPointer < imgPeopleSrcArrayLength - 1 ? imgPointer++ : imgPointer = 0;
        }
    }

    function imgCreator(src, wereSelector = "") {
        const img = document.createElement("img");
        img.setAttribute("src", src);
        img.setAttribute("alt", "image");
        if (wereSelector !== "") {
            document.querySelector(wereSelector).append(img);
        }
        return img;
    }

    function loadSlider() {
        for (let i = 0; i < 6; i++) {
            const img = imgCreator(imgPeopleSrcArray[i], ".people-say-img-box");
            img.style.left = 100 * (i - 1) + "px";
            if(i===3){
                img.style.top = "0";
            }
            nextPointer();
            loadCurrentPersonImg();
        }
        activeCurrentPersonInfo();
    }

    function activeCurrentPersonInfo() {
        const quotes = document.querySelector(".people-say-quote");
        const names = document.querySelector(".people-say-person-name");
        const professions = document.querySelector(".people-say-person-profession");
        let index = imgPointer < 3 ? imgPeopleSrcArrayLength + imgPointer - 2 : imgPointer - 2;

        deleteActive(quotes, names, professions);
        hiddenToActive(quotes, names, professions);

        function hiddenToActive(...nodes) {
            nodes.forEach(node => {
                node.querySelector(`p:nth-child(${index})`).classList.replace("hidden", "active")
            })
        }

        function deleteActive(...nodes) {
            nodes.forEach(node => {
                if (node.querySelector(".active"))
                    node.querySelector(".active").classList.replace("active", "hidden");
            })
        }
    }
}
