"use strict"
let number = getConsoleNumber();
console.log(factorial(number));

function getConsoleNumber() {
    let number = prompt("Enter a number");
    while (!number || !Number.isInteger(+number)) {
        number = prompt("Enter correct number", number);
    }
    return +number;
}

function factorial(arg) {
    if (!Number.isInteger(arg) || arg < 0) {
        return "factorial error";
    }
    return arg > 0 ? arg * factorial(arg - 1) : 1;
}