"use strict"

function createNewUser() {
    return {
        _firstName: getConsoleText("user first name"),
        _lastName: getConsoleText("user last name"),
        _birthday: getConsoleText("user birthday (dd.mm.yyyy)", 10),
        getLogin() {
            return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
        },
        set setFirstName(firstName) {
            this._firstName = firstName;
        },
        set setLastName(lastName) {
            this._lastName = lastName;
        },
        getAge() {
            const birthDayArr = this._birthday.split('.')
            let birthDay = (birthDayArr[1] - 1).toString() + birthDayArr[0];
            let currentDay = new Date().getMonth().toString() + new Date().getDate();
            if (+birthDay <= +currentDay) {
                return new Date().getFullYear() - birthDayArr[2];
            } else {
                return new Date().getFullYear() - birthDayArr[2] - 1;
            }
        },
        getPassword() {
            return this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + this._birthday.split(".")[2];

        },
    }
}

function getConsoleText(whatToGet = "some text", textLength = 2) {
    let text;
    do {
        text = prompt("Please input " + whatToGet);
    } while (!text || text.trim().length < textLength)
    return text;
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
