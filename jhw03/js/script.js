"use strict"
//Input number

let number;
do {
    number = prompt("input an integer");
} while (!number || !Number.isInteger(+number))
number = +number;

//way #1

// const multipleBase = 5;
// if (number >= multipleBase) {
//     for (let i = 0; i <= number; i += multipleBase) {
//         console.log(i);
//     }
// } else {
//     console.log("Sorry, no numbers");
// }

//way #2

let isMultiply = false;
for (let i = 0; i <= number; i++) {
    if(i%5 === 0) {
        console.log(i);
        isMultiply = true;
    }
}
if(!isMultiply) console.log("Sorry, no numbers");

//Прості числа необов'язкова частина
// input number
let number1 = prompt("input a positive number #1");
while(!number1 || !Number.isInteger(+number1) || +number1 < 1){
    number1 = prompt("Incorrect! Input a positive number #1");
}
number1 = +number1;

let number2 = prompt("input a positive number #2");
while(!number2 || !Number.isInteger(+number2) || +number2 < 1){
    number2 = prompt("Incorrect! Input a positive number #2");
}
number2 = +number2;

//min max
let min;
let max;
if(number1 > number2) {
    min = number2;
    max = number1;
} else{
    min = number1;
    max = number2 ;
}

console.log("Прості числа в діапазоні від " + min + " до " + max);
for (let i = min; i <= max; i++) {
    let  isDenominator = false;
    for (let j = 2; j < i; j++) {
        if(i % j === 0){
            isDenominator = true;
            break;
        }
    }
    if(!isDenominator) console.log(i);
}



