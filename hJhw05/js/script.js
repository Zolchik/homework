'use strict'
const usersUrl = 'https://ajax.test-danit.com/api/json/users';
const postsUrl = 'https://ajax.test-danit.com/api/json/posts';

(async () => {
    const postsContainer = document.querySelector('.posts')

    const [users, posts] = await Promise.all([await (await fetch(usersUrl)).json(), await (await fetch(postsUrl)).json()]);
    posts.forEach(({title, body, userId, id}) => {
            const {name, email} = users.find(user => user.id === userId);
            const card = new Card(title, name, email, body, id)
            card.add(postsContainer);
    })

    document.querySelector('.posts').addEventListener('click', async (e) => {
        if (!e.target.classList.contains('fas')) return;
        const currentId = e.target.parentNode.getAttribute('data-id');
        const response = await fetch(postsUrl + '/' +currentId, {
            method: 'DELETE',
        });
        if (response.status === 200) {
            Card.remove(currentId);
        }
    })

})();

class Card {
    #name
    #email
    #title
    #text
    #html
    #id

    constructor(title, user, email, text, id) {
        this.#name = user;
        this.#email = email;
        this.#title = title;
        this.#text = text;
        this.#id = id;
        this.#html = this.#render();
    }

    #render() {
        const divCard = document.createElement('div');
        divCard.classList.add('card');
        divCard.setAttribute('data-id',this.#id);
        const iIcon = document.createElement('i');
        iIcon.classList.add('fas', 'fa-trash-alt');
        divCard.append(iIcon);
        let h4 = document.createElement('h4');
        h4.innerText = this.#title
        divCard.append(h4);
        const divInfo = document.createElement('div');
        divInfo.classList.add('card-info');
        divCard.append(divInfo);
        let span = document.createElement('span');
        span.classList.add('card-info_user-name');
        span.innerText = this.#name
        divInfo.append(span);
        let i = document.createElement('i');
        i.classList.add('card-info_user-email');
        i.innerText = this.#email
        divInfo.append(i);
        let p = document.createElement('p');
        p.classList.add('card-text');
        p.innerText = this.#text;
        divCard.append(p);
        return divCard;
    }
    add(node) {
        node.append(this.#html);
    }

    static remove(id){
        document.querySelector(`[data-id='${id}']`).remove();
    }
}
