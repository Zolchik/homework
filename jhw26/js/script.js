"use strict"

document.querySelector(".pre-button").addEventListener("click", () => {
    moveRight();
    removeImg("last-child");
    addImg("begin");
})

document.querySelector(".next-button").addEventListener("click", () => {
    moveLeft();
    removeImg("first-child");
    addImg("end");
})

let srcArray = [];
let currentImgIndex = 0;
let step = 0;
const sliderWidth = 500;

removeAllImg();
for (let i = 0; i < 3; i++) addImg();

function removeAllImg() {
    document.querySelectorAll(".img-wrapper").forEach((element, index) => {
        let innerElement = element.querySelector("*");
        srcArray[index] = innerElement.getAttribute("src");
        element.remove();
    })
}

function addImg(position = "end") {
    let divImg = document.createElement("div");
    divImg.classList.add("img-wrapper");
    let img = divImg.appendChild(document.createElement("img"));
    img.setAttribute("alt", "The Simpsons");
    img.setAttribute("src", srcArray[currentImgIndex]);
    if (position === "end")
        document.querySelector(".slider").append(divImg);
    if (position === "begin")
        document.querySelector(".slider").prepend(divImg);
    currentImgIndex = currentImgIndex + 1 < srcArray.length ? currentImgIndex + 1 : currentImgIndex = 0;
    divImg.style.left = sliderWidth * (step - 1) + "px";
    step = (step + 1) % 3;
}

function removeImg(child) {
    document.querySelector(`.img-wrapper:${child}`).remove();
}

function moveLeft() {
    document.querySelectorAll(".img-wrapper").forEach((element) => {
        element.style.left = parseInt(element.style.left) - sliderWidth + "px";
    })
    step = 2;
}

function moveRight() {
    document.querySelectorAll(".img-wrapper").forEach((element) => {
        element.style.left = parseInt(element.style.left) + sliderWidth + "px";
    })
    step = 0;
}