"use strict"

function arrayDisplay(array, where = document.body) {
    where.insertAdjacentHTML("beforeend", createHTML(array));
}
function createHTML(array) {
    return "<ul>" + array.map(el=> Array.isArray(el)
        ? `<li>${createHTML(el)}</li>`
        : `<li>${el}</li>`
    ).join("") + "</ul>"
}

function clearPage() {
    document.body.querySelectorAll("body>*:not(js)").forEach(element => element.remove());
}

function counting() {
    let timeDown = 3;
    let countingDiv = document.body.appendChild(document.createElement("div"))
    countingDiv.style.cssText = `margin: 0 auto; font-size: 50px; text-align: center; color: blue`;
    setTimeout(function run() {
        countingDiv.innerText = timeDown;
        timeDown-- > 0 ? setTimeout(run, 1000) : clearPage();
    });
}

arrayDisplay(["hello", "world", "Kyiv", ["Borispol", ["airport", "city"], "Irpin"], "Kharkiv", "Odessa", "Lviv"]);
// arrayDisplay(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
counting();



// function arrayDisplay(array, where = document.body) {
//     let list = where.appendChild(document.createElement("ul"));
//
//     array.forEach(el => {
//         if (Array.isArray(el)) {
//             arrayDisplay(el,list.lastChild)
//         } else {
//             list.appendChild(document.createElement("li")).innerText = el;
//         }
//     })
// }
// function arrayDisplay(array, where = document.body) {
//  // where.insertAdjacentHTML("beforeend", "<ul>" + array.map(el =>`<li>${el}</li>`).join("")+"</ul>");
// }


