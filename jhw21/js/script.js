"use strict"

function clearBody() {
    document.querySelectorAll("body *").forEach(element => element.remove());
}

function createRound(diameter) {
    let round = document.createElement("div");
    round.style.borderRadius = "50%";
    round.style.backgroundColor = randomColor();
    round.style.width = round.style.height = diameter + "px";
    return round;
}

function randomNumber(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function randomColor() {
    return `rgb(${randomNumber(0, 255)},${randomNumber(0, 255)},${randomNumber(0, 255)})`
}

document.querySelector("button").addEventListener("click", () => {
    clearBody();
    let input = document.body.appendChild(document.createElement("input"));
    input.setAttribute("placeholder", "діаметр кола")
    let button = document.body.appendChild(document.createElement("button"));
    button.innerText = "Намалювати";
    button.addEventListener("click", () => {
        const diameter = document.querySelector("input").value;
        clearBody();
        let board = document.body.appendChild(document.createElement("div"));
        board.classList.add("board");
        for (let i = 0; i < 100; i++) {
            board.appendChild(createRound(diameter));
        }
        board.addEventListener("click", (e) => {
            if (!e.target.classList.contains("board")) {
                e.target.remove();
            }
        })
    })
})