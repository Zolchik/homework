'use strict'
const url = 'https://ajax.test-danit.com/api/swapi/films';

(async () => {
    const filmsList = await (await fetch(url)).json();
    const filmCards = document.querySelector('.film-cards');

    filmsList.map(async ({episodeId, name, openingCrawl, characters}) => {

        filmCards.insertAdjacentHTML("beforeend", ` <div class="card">
            <h4 >Епізод ${episodeId}</h4>
            <h5 >${name}</h5>
            <ul id = 'episode${episodeId}'></ul>
            <p>${openingCrawl}</p></div>`);

        const charactersList = document.querySelector(`#episode${episodeId}`);

        //персонажі підгружаються поступово
        // characters.map(async (url) => {
        //     const character = await (await fetch(url)).json();
        //     charactersList.insertAdjacentHTML("beforeend", `<li>${character.name}</li>`);
        // })

        // персонажі  підгружаються цілим списком
        charactersList.innerHTML = (await Promise.allSettled(
                characters.map(async (url) => {
                    const character = await (await fetch(url)).json();
                    return character.name;
                })
            )
        ).reduce((html, el) => html += `<li>${el.value}</li>`, '');

    })
})();