'use strict'
const url = 'https://ajax.test-danit.com/api/swapi/films';

(async () => {
    const filmsList = await (await fetch(url)).json();
    const filmCards = document.querySelector('.film-cards');

    filmsList.map(async ({episodeId, name, openingCrawl, characters}) => {

        const card = new Card(episodeId, name, openingCrawl);
        filmCards.append(card.html());

        card.charactersList = await Promise.all(
            characters.map(async (url) => {
                const character = await (await fetch(url)).json();
                return character.name;
            })
        );
    })
})();

class Card {
    #card;
    #episodeId;
    #episodeName;
    #charactersList;
    #openingCrawl;

    constructor(episodeId = '', episodeName = '', openingCrawl = '', charactersList = []) {
        this.#creat();
        this.episodeId = episodeId;
        this.episodeName = episodeName;
        this.charactersList = charactersList;
        this.openingCrawl = openingCrawl;
    }

    html() {
        return this.#card;
    }

    #creat() {
        const tag = document.createElement('div');
        tag.classList.add('card');
        this.#card = tag;
    }

    set episodeId(episodeId) {
        const tag = document.createElement('h4');
        tag.innerText = 'Епізод ' + episodeId;
        this.#card.append(tag);
        this.#episodeId = tag;
    }

    set episodeName(episodeName) {
        const tag = document.createElement('h5');
        tag.innerText = episodeName;
        this.#card.append(tag);
        this.#episodeName = tag;
    }

    set charactersList(charactersList) {
        const tag = document.createElement('ul');
        tag.innerHTML = charactersList.reduce((liCharacters, character) => liCharacters + `<li>${character}</li>`, '')
        this.#episodeName.after(tag);
        this.#charactersList = tag;
    }

    set openingCrawl(openingCrawl) {
        const tag = document.createElement('p');
        tag.innerText = openingCrawl;
        this.#card.append(tag);
        this.#openingCrawl = tag;
    }
}