"use strict"

function changeStyle() {
    const styleTag = document.querySelector(`[href*="css/style"]`);
    if (styleTag.getAttribute('href') === "scss/style-dark.scss") {
        styleTag.setAttribute('href', "scss/style.scss")
        localStorage.setItem('href', "scss/style.scss");
    } else {
        styleTag.setAttribute('href', "scss/style-dark.scss")
        localStorage.setItem('href', "scss/style-dark.scss");
    }
}

if (localStorage.getItem('href')) {
    document.querySelector(`[href*="css/style"]`).setAttribute('href', localStorage.getItem('href'))
}
document.body.querySelector(".theme").addEventListener("click", changeStyle)