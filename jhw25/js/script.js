"use strict"

const display = document.body.querySelector(".display>input");
let firstOperand = "";
let operand = "";
let secondOperand = "";
let result = "";
let m = "";
let mrc = 0;
display.value = 0;

document.body.querySelector(".keys").addEventListener("click", e => {
    if(!e.target.classList.contains("button")) return ;
    const value = e.target.value;
    document.querySelector(".memory").style.display = "none";

    if (value === "C") return pressC();

    if (value === "m+" || value === "m-") return pressM(value);

    if (value === "mrc") return pressMrc();
    mrc = 0;

    if ((parseFloat(value) >= 0 || (value === "." && !firstOperand.includes("."))) && operand === "")
        return firstOperand = readNumber(value, firstOperand);

    if (value === "+" || value === "-" || value === "*" || value === "/")
        return pressOperand(value);

    if (parseFloat(value) >= 0 || (value === "." && !secondOperand.includes(".")))
        return secondOperand = readNumber(value, secondOperand);

    if (value === "=") return pressEqual();
})

function readNumber(char, writeIn) {
    writeIn += char;
    return display.value = writeIn;

}

function pressC() {
    firstOperand = "";
    operand = "";
    secondOperand = "";
    result = ""
    display.value = 0;
}

function pressMrc() {
    operand = "";
    if (m === "") return;
    mrc++;
    if (mrc === 1) {
        display.value = m;
        result = '';
    } else {
        m = "";
        mrc = 0;
        display.value = 0
    }
}

function pressM(char) {
    operand = "";
    firstOperand = secondOperand;
    secondOperand = "";
    if (!parseFloat(display.value)) return;
    document.querySelector(".memory").style.display = "block";
    if (m === "") m = display.value;
    else if (char === "m+") m = parseFloat(m) + parseFloat(display.value);
    else m = parseFloat(m) - parseFloat(display.value);
}

function pressEqual() {
    if (secondOperand !== "") {
        if (firstOperand !== "")
            result = mathOperation(firstOperand, secondOperand, operand);
        else if (result !== "")
            result = mathOperation(result, secondOperand, operand);
        else
            result = mathOperation(0, secondOperand, operand);
    }

    display.value = result;
    if (!parseFloat(result)) result = "";
    firstOperand = "";
    operand = "";
    secondOperand = "";
}

function pressOperand(char) {
    if (secondOperand !== "") {
        if (firstOperand !== "") {
            result = mathOperation(firstOperand, secondOperand, operand);
            firstOperand = "";
        } else if (result !== "")
            result = mathOperation(result, secondOperand, operand);
        display.value = result;
        if (!parseFloat(result)) result = "";
        secondOperand = "";
    }
    operand = char;
}

function mathOperation(firstOperand, secondOperand, operand) {
    firstOperand = +firstOperand;
    secondOperand = +secondOperand;

    switch (operand) {
        case "+":
            return firstOperand + secondOperand;
        case "-":
            return firstOperand - secondOperand;
        case "*":
            return firstOperand * secondOperand;
        case "/":
            return secondOperand === 0 ? "Can't divide by zero" : firstOperand / secondOperand;
    }
}